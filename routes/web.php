<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// simple route
Route::get('/home',function (){
    return "Home Page";
});

route::get("/Home", function(){
    return "HOME PAGE";
});

//Route Parameters

Route::get('user/{u_id}',function($id){
    return $id;
});
Route::get('user/{u_id}/name/{u_name}', function($uid,$uname){
    return $uid."&nbsp;".$uname;
});

//Optional Parameters

Route::get('student/{name?}', function($name=null){
    return "Hellow " . $name;
});

Route::get('name/{name?}',function($name='Enter Parameter Name'){
    return "Hellow " .$name;
});

// Regular Expression 

Route::get('product/{p_name}', function($pname){
    return "Product Name : " . $pname;
})->where('p_name','[A-Za-z]+');

Route::get('manager/{id}/{name}', function($id,$name){
    return "Manager ID : ". $id ." Manager Name : ". $name;
})->where(['id' => '[0-9]+', 'name' => '[a-z]+']);

// Regular Expression helper method

Route::get('emplyee/{id}/{name}', function($id,$name){
    return "Manager ID : ". $id ." Manager Name : ". $name;
})->whereNumber('id')->whereAlpha('name');

// Redirect Routes

Route::Redirect('aniruddh','anirudhdh');
Route::Redirect('krishna','aniruddh',301);
Route::permanentRedirect('mahadev','anu');

// Fallback Routes

Route::Fallback(function(){
    return "This Route is not defend";
});